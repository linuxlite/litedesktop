Lite Desktop
================

Lite Desktop is a convenient way for you to get to frequently accessed locations on your computer.

## Screenshot:

![](https://imgur.com/lOzI3QP.png)

## License ![License](https://img.shields.io/badge/license-GPLv2-green.svg)

This project is under the GPLv2 license. Unless otherwise stated in individual files.

## Authors
- [Jerry Bezencon](https://gitlab.com/linuxlite/)
